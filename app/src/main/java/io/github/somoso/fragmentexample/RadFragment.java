package io.github.somoso.fragmentexample;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class RadFragment extends Fragment {

    public RadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rad, container, false);

        Button letsGoHomeLads = (Button) view.findViewById(R.id.jobDone);
        letsGoHomeLads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Because we used addToBackStack() on StartFragment when transitioning to this, we
                // can just do popBackStack() to get to the previous one.
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        return view;
    }


}
