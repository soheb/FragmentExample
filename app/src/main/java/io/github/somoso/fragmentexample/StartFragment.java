package io.github.somoso.fragmentexample;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class StartFragment extends Fragment {

    public StartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_start, container, false);

        Button fragmentChange = (Button)rootView.findViewById(R.id.changeFragment);
        fragmentChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the transaction
                getActivity().getSupportFragmentManager().beginTransaction()
                        // Replace whatever is in the container holding the existing
                        // fragment with our new Fragment
                        .replace(R.id.container, new RadFragment(), "TestFragmentTag")
                        // Add our current Fragment to the back stack (without giving this state a
                        // name).
                        // Note - this also allows the user to press the back button to get to this
                        // Fragment
                        .addToBackStack(null)
                        // Commit the fragment change!
                        .commit();
            }
        });

        return rootView;
    }
}
