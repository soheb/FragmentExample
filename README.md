FragmentExample
===============

Just a basic project I did on teaching myself how to transition between Android fragments, and how
to revert back.

How to use
----------
Import it into Android Studio as a gradle project, and then build and run it. Tested on:

* Genymotion Nexus 4 emulated device

* Real Nexus 4 device

Acknowledgements
----------------

I got the icon from Uzicopter's fantastic Tumblr blog:

[http://uzicopter.tumblr.com/post/105045891049/pineapplesandsalt-happy-aloha-friday-sunshines]
(http://uzicopter.tumblr.com/post/105045891049/pineapplesandsalt-happy-aloha-friday-sunshines)

I got the "totally_rad.jpg" file from a Google Image search for the artist Fantastisizer:

[http://i.ytimg.com/vi/LYtJkbomqq4/maxresdefault.jpg]
(http://i.ytimg.com/vi/LYtJkbomqq4/maxresdefault.jpg)

The original image is a thumbnail for a youtube video of one of his songs (Breaking Point).

I'm only using them for educational purposes. If the original artists have a problem with this,
please contact me at soheby /at gmail \dot com with the slashes gone and the words at and dot
replaced with their actual characters, and I will have the images/references removed/changed as soon
as possible.